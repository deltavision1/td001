import pickle
import os 
from string import ascii_letters
from random import choice, randint

def Save_object(file, object):
	with open(str(file), "wb") as fichier:
		mon_pickler = pickle.Pickler(fichier)
		mon_pickler.dump(object)

def load_object(file):
	with open(str(file), "rb") as fichier:
		mon_Unpickler = pickle.Unpickler(fichier)
		score = mon_Unpickler.load()
		return score


def create_file():
	try :
		load_object("./Data/data")
	except FileNotFoundError:
		score = dict()
		try:
			os.mkdir("./Data")
		except FileExistsError:
			pass
		Save_object("./Data/data", score)
		pass

def test_create_user(name):
	if name in load_object("./Data/data"):
		print("reboujour {}".format(name))
		score = load_object("./Data/data")
	else:
		print("bienvenue {} on vas crée votre user !".format(name))
		score = load_object("./Data/data")
		score[name] = 0
		Save_object("./Data/data", score)
	return score

def game():
	#generation du mot aleatoirement 
	#--------------------------------------------------------------
	i = randint(2,8)
	random_string =[choice(ascii_letters).lower() for i in range(i)]
	random_string_hide = ['*' for i in range(i)]
	random_string_hide_result = ''.join(random_string_hide)
	print(random_string_hide_result)
	#--------------------------------------------------------------
	n = 8 #attempt number
	test_1 = True  #test_1 : condition d'arrete de la boucle while
	while n > 0 and test_1:
		test = True #test : test de l'existance du caractère dans la mot cachée.
		try:
			a = input("try : ")
		except ValueError:
			print("veuillez Saisir une valeur valide !!")
			continue
		for j in range (i) :
			if a == random_string[j]:
				random_string_hide[j] = a
				test = False 

		random_string_hide_result = ''.join(random_string_hide)
		print(random_string_hide_result)
		n -= 1 if test else 0
		if random_string_hide == random_string :
			print("end ! ")
			test_1 = False
	finale = ''.join(random_string)
	print("le mot étais {} ".format(finale))
	return n

